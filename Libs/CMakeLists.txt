project(StackProj)
add_library(stack SHARED ${PROJECT_SOURCE_DIR}/stack.c)
target_include_directories(stack PUBLIC ${PROJECT_SOURCE_DIR})

option(ENABLE_COVERAGE "Enable coverage" ON)

if(ENABLE_COVERAGE)
  if(CMAKE_CXX_COMPILER_ID MATCHES GNU)
    message(STATUS "Coverage analysis with gcov enabled") 
    target_compile_options(stack
      PUBLIC
        -fprofile-arcs -ftest-coverage -g
      )
    target_link_libraries(stack
      PUBLIC
        gcov
      )
  else()
    message(WARNING "Coverage not supported for this compiler")
  endif()
endif()
