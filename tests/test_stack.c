#include <stdarg.h>
#include <stddef.h>
#include <setjmp.h>
#include <stdlib.h>
#include <stdint.h>
#include <cmocka.h>
#include "stack.h"

#define UNUSED(x) (void)(x)
#define STRESS_TEST_ITERATIONS (100)

/* Declare the variables your tests want to use. */
static Stack s;

/** These functions will be used to initialize
   and clean resources up after each test run 
*/
int setup (void **state){
    UNUSED(state);
    s = stack_create();
    assert_non_null(s);
    return 0;
}

int teardown (void **state){
    UNUSED(state);
    stack_destroy(s);
    return 0;
}

/**
 * assert_* versions generate fatal failures when they fail, and abort the current function. 
 */

void BasicTest(void **state){
    UNUSED(state);
    stack_push(s, 1);
    stack_push(s, 2);
    stack_push(s, 3);
    assert_int_equal(stack_isempty(s), 0);
    assert_int_equal(stack_pop(s), 3);
    assert_int_equal(stack_isempty(s), 0);
    assert_int_equal(stack_pop(s), 2);
    assert_int_equal(stack_isempty(s), 0);
    assert_int_equal(stack_pop(s), 1);
    assert_int_equal(stack_isempty(s), 1);
    assert_int_equal(stack_pop(s), STACK_EMPTY);
    assert_int_equal(stack_isempty(s), 1);
    assert_int_equal(stack_pop(s), STACK_EMPTY);
}

void StressTest(void **state){
    UNUSED(state);
    for(int i=0; i<STRESS_TEST_ITERATIONS; i++){
      stack_push(s, 2*i);
      assert_int_equal(stack_isempty(s), 0);
    }
    for(int j = STRESS_TEST_ITERATIONS-1; j>=0; j--){
      assert_int_equal(stack_pop(s), 2*j);
    }
    assert_int_equal(stack_pop(s), STACK_EMPTY);
    assert_int_equal(stack_isempty(s), 1);
}

int main(void)
{
  const struct CMUnitTest tests[] = {
    cmocka_unit_test(BasicTest),
    cmocka_unit_test(StressTest),
  };
  return cmocka_run_group_tests(tests, setup, teardown);
}
