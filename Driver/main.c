#include <stdio.h>
#include "stack.h"

int main(void){
    printf("Hello Gitlab CI!\n");
    Stack s = stack_create();
    stack_push(s, 1);
    stack_destroy(s);
    return 0;
}
